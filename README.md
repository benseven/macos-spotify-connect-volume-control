## Controlling Spotify's Volume with a Keyboard Shortcut even when using Spotify Connect to Stream to a Speaker ##


### Why? ###

When using Spotify on MacOS and streaming to a wifi speaker like Sonos or the Amazon Echo using Spotify Connect, the Mac's volume control keys control your Mac's audio output, rather than Spotify's volume on your speaker. 

### Help and Support ###

This works for me on macOS Mojave, and Spotify, as of 27th April 2020. 

Update - and yes, this was a big jump - I then made some changes to it in 2023 for macOS Sonoma, and it's working well.

I can't help you, if it doesn't work for you. Sorry.

### So what do I do? ###

1. Create an Automator 'Quick Action' system service for 'Volume Up' that uses Applescript to send Spotify its volume up keyboard shortcut
2. Create an Automator 'Quick Action' system service for 'Volume Down' that uses Applescript to send Spotify its volume up keyboard shortcut
3. Bind those Automator actions to keyboard shortcuts in your Keyboard preferences

Along the way, we also have to grant Automator access to send keyboard shortcuts in your Mac's Accessibility settings - but Automator prompts you when it needs to do this.

### Phase 1 - Volume Up action ###

1. Open 'Automator' from your Mac's 'Applications' folder
2. Start a new document, with the type 'Quick Action'
3. At the top, choose the 'Workflow receives' option 'no input', which is at the bottom of the drop-down menu
4. From the 'Utilities' folder in Automator's sidebar, drag 'Run Applescript' into the main part of the window
5. Delete the Applescript code that is in this new action
6. Paste in the following code:

```
# macOS Sonoma? Get rid of the line below, on any version of macOS where it's making Spotify come to the front when you do this.
do shell script "open -g /Applications/Spotify.app"

tell application "Spotify"
	set currentvol to get sound volume
	set sound volume to currentvol + 10
end tell

```
The first line 'activates' Spotify without bringing it into focus every time we run the command - this was needed on Mojave but made Spotify come to the front on Sonoma, so delete the line if that's happening to you;
The second part gets Spotify's currently set volume, and increments it by 10%. If you'd like a bigger or smaller increment, just change that to e.g. 20 for a 20% increase.

7. Above the Applescript, press the play button; you might be prompted to open Accessibility settings to allow Automator to be able to send Keyboard shortcuts - if you are, just follow the prompts and tick the checkbox next to Automator after entering your password.

8. Back in Automator, save your workflow - I called mine 'Spotify Connect Volume Up'.

### Phase 2 - Volume down action ###

Repeat all the steps from Step 1, with a few exceptions:

- In step 6, change the `+` in the code to a `-` (minus) so the volume is turned down instead of up;

- Skip step 7, as you've already made sure Automator can send keyboard shortcuts in Phase 1.

- In step 8, give your workflow a different name, like 'Spotify Connect Volume Down'.

### Phase 3 - Setting up a Keyboard Shortcut for each.

Credit goes to this [Addictive Tips article](https://www.addictivetips.com/mac-os/run-an-applescript-with-a-keyboard-shortcut-on-macos/) for the method:

1. Open System Preferences > Keyboard

2. Click the 'Shortcuts' tab

3. Choose 'Services' from the left hand menu

4. Scroll down the list to 'General' and find your Volume Up action - whatever name you gave to it

5. Where it says 'None' to the right of the action name, click and then click 'Add shortcut', which will give you a text box with a flashing cursor

6. Do the keyboard shortcut you want for Volume up. I chose Ctrl+Command+Up, because that's the same command as if you had Spotify open in front of you, at least on Mojave - and I couldn't think of anything it would clash with on my system. If the shortcut you use here makes anything else happen on your computer, switch back to system preferences and repeat step 5 and 6 using a different keyboard shortcut.

7. If set successfully, you'll only see the shortcut listed next to the action name if you click on a different action in this list, otherwise it's the 'add shortcut' button again. You're welcome, and big thanks to the UI engineer that tested that one.

8. Repeat steps 4, 5 and 6 for your Volume Down action too.

Try it out! You should now be able to control your Spotify Connect volume from any app. Some apps, the first time you run each shortcut, might ask for permission to control Spotify. There is a slight lag in the volume changing, but it's totally worth it for the convenience and lack of interrupting work to adjust volume in a UI. 

If this was useful to you, tweet me `@ben_seven` and say hi :) Don't tweet me if it didn't work, because I can't help you.